package ut.com.bhaliniak.boardPriority;

import org.junit.Test;
import com.bhaliniak.boardPriority.api.MyPluginComponent;
import com.bhaliniak.boardPriority.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}